console.log(__filename)
console.log(__dirname)
const EventEmitter = require('events');
const emitter = new EventEmitter()
var url = 'http://mylogger.io/log';
function log(message) {
    // send http request
    console.log(message )
    emitter.emit('MessageLogged', {id:1,url:'http://'})
}

module.exports = log;
module.exports.log2 = log;

// Module Wrapper Function: Node doesn't directly executes our code
// instead it wraps our code in a function that function is called as modeule Wrapper Function.