const logger = require('./logger.js')
function sayHello(name) {
    console.log("Hello "+name);
}
//sayHello('Chetan')

var message ='';
console.log(global.message)

logger('message')
console.log(logger.log2)

// path module in node
const path = require('path');
var pathObj = path.parse(__filename) 
console.log(pathObj);

//OS Module
const os = require('os');
var totalMemory = os.totalmem();
console.log("Total Memory of machine: "+totalMemory)
var freeMemory = os.freemem() 
console.log(`Free Memory: ${freeMemory}`)

//File Systems Module
const fs = require('fs');
//Synchronous Function
//const files = fs.readdirSync('./');
//console.log(files);

//Async Function
//fs.readdir('$/', function (err, files) { err ? console.log("Error",err) : console.log("Result",files)})

//Events Module
const EventEmitter = require('events');
const { emit } = require('process');
const emitter = new EventEmitter()
//Register a Listener
emitter.on('MessageLogged', (arg) => console.log('Listener Called',arg))
//emit method used to raise an Event
//emitter.emit('MessageLogged',{id:1 , url:'http://'})

const log = require('./logger')
log('message')

//HTTP Module
const http = require('http');
const { constants } = require('buffer');
const server = http.createServer((req, res) => {
    if(req.url === '/'){
    res.write("Hello World")
    res.end();
    }
    if(req.url === '/api/courses'){
        res.write(JSON.stringify([1,2,3,4,5]))
        res.end()
    }
})
server.on('connection', (socket) => {
    console.log("New Connection")
})
server.listen(7002)
console.log("Server is listening on port 7002...")

